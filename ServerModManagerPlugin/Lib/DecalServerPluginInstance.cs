﻿using Decal.Adapter;
using System.Reflection;
using System;
using static GameMessageQueryServerPluginsResponse;
using System.IO;

namespace ServerModManager.Lib {
    internal class DecalServerPluginInstance : ServerPluginInstanceBase {
        private Assembly pluginAssembly;
        private Type pluginType;
        private object pluginInstance;

        public DecalServerPluginInstance(PluginDescription description, byte[] data) : base(description, data) {
        }

        public override void Run() {
            pluginAssembly = Assembly.Load(Data);
            pluginType = pluginAssembly.GetType($"{Description.Id}.PluginCore");
            pluginInstance = Activator.CreateInstance(pluginType);

            var startupMethod = pluginType.GetMethod("Startup", BindingFlags.NonPublic | BindingFlags.Instance);
            startupMethod.Invoke(pluginInstance, new object[] { });
        }

        public override void Dispose() {
            if (pluginInstance != null && pluginType != null) {
                MethodInfo shutdownMethod = pluginType.GetMethod("Shutdown", BindingFlags.NonPublic | BindingFlags.Instance);
                shutdownMethod.Invoke(pluginInstance, null);
                pluginInstance = null;
                pluginType = null;
                pluginAssembly = null;
            }
        }
    }
}