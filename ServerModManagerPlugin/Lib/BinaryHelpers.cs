﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerModManager.Lib {
    internal class BinaryHelpers {
        /// <summary>
        /// Read an ascii string from the buffer. Automatically aligns to DWORD afterwards.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static string ReadString(BinaryReader buffer) {
            long position = buffer.BaseStream.Position;
            int num = buffer.ReadInt16();
            if (num == -1) {
                num = buffer.ReadInt32();
            }

            byte[] bytes = buffer.ReadBytes(num);
            int num2 = (int)((buffer.BaseStream.Position - position) % 4);
            if (num2 > 0) {
                buffer.ReadBytes(4 - num2);
            }

            return Encoding.ASCII.GetString(bytes);
        }

        /// <summary>
        /// Read a WString (unicode) from the buffer
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static string ReadWString(BinaryReader buffer) {
            int num = buffer.ReadByte();
            if (((uint)num & 0x80u) != 0) {
                num = ((num & 0x7F) << 8) | buffer.ReadByte();
            }

            byte[] bytes = buffer.ReadBytes(num * 2);
            return Encoding.Unicode.GetString(bytes);
        }
    }
}
