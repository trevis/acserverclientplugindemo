﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting;
using UtilityBelt.Service;
using static GameMessageQueryServerPluginsResponse;

namespace ServerModManager.Lib {
    public class LuaServerPluginInstance : ServerPluginInstanceBase {
        private UBScript? script;

        public LuaServerPluginInstance(PluginDescription description, byte[] data) : base(description, data) {

        }

        public override async void Run() {
            var data = Encoding.UTF8.GetString(Data);
            await UBService.Scripts.StartScript(Description.Id, false);
            script = UBService.Scripts.GetScript(Description.Id);
            script.RunText(data);
        }

        public override void Dispose() {
            UBService.Scripts.StopScript(Description.Id);
        }
    }
}
