﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GameMessageQueryServerPluginsResponse;

namespace ServerModManager.Lib {
    public abstract class ServerPluginInstanceBase : IDisposable {
        public PluginDescription Description { get; }
        public byte[] Data { get; }

        public ServerPluginInstanceBase(PluginDescription description, byte[] data) {
            Description = description;
            Data = data;
        }

        public abstract void Run();

        public abstract void Dispose();
    }
}
