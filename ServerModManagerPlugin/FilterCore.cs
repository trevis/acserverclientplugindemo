﻿using Decal.Adapter;
using ServerModManager.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UtilityBelt.Service;
using static GameMessageQueryServerPluginsResponse;

namespace ServerModManager {
    [FriendlyName("ServerMods.Loader")]
    public class FilterCore : FilterBase {
        private ServerModManagerUI ui;

        public List<PluginDescription> AvailablePlugins { get; private set; } = new List<PluginDescription>();
        public Dictionary<string, ServerPluginInstanceBase> InstalledPlugins { get; } = new Dictionary<string, ServerPluginInstanceBase>();


        /// <summary>
        /// Assembly directory (contains both loader and plugin dlls)
        /// </summary>
        public static string AssemblyDirectory => System.IO.Path.GetDirectoryName(Assembly.GetAssembly(typeof(FilterCore)).Location);

        public DateTime LastDllChange { get; private set; }
        public bool IsLoggedIn { get; private set; }

        #region Event Handlers
        protected override void Startup() {
            try {
                Core.PluginInitComplete += Core_PluginInitComplete;
                Core.PluginTermComplete += Core_PluginTermComplete;
                Core.FilterInitComplete += Core_FilterInitComplete;
                Core.RenderFrame += Core_RenderFrame;
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private void Core_RenderFrame(object sender, EventArgs e) {
            try {
                Core.RenderFrame -= Core_RenderFrame;
                ui = new ServerModManagerUI(this);
                UBService.Scripts.MessageHandler.Incoming.UnknownMessageType += Incoming_UnknownMessageType;
                UBService.Scripts.MessageHandler.Outgoing.Login_SendEnterWorldRequest += Outgoing_Login_SendEnterWorldRequest;
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private void Outgoing_Login_SendEnterWorldRequest(object sender, UtilityBelt.Common.Messages.Events.Login_SendEnterWorldRequest_C2S_EventArgs e) {
            try {
                IsLoggedIn = true;
                var needsToLoad = InstalledPlugins.Values.Where(p => p.Description.LoadType == PluginLoadType.LoggedIn);

                foreach (var plugin in needsToLoad) {
                    plugin.Run();
                }
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        public void RemovePlugin(string id) {
            if (InstalledPlugins.TryGetValue(id, out var pluginInstance)) {
                InstalledPlugins.Remove(id);
                pluginInstance.Dispose();
            }
        }

        private void Core_FilterInitComplete(object sender, EventArgs e) {
            try {
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private void Incoming_UnknownMessageType(object sender, UtilityBelt.Common.Messages.Events.UnknownMessageTypeEventArgs e) {
            try {
                if (e.Type == GameMessageQueryServerPluginsResponse.Type) {
                    var response = new GameMessageQueryServerPluginsResponse(e.RawData);
                    Log($"Got a list of {response.AvailablePlugins.Count} server plugins:");
                    foreach (var plugin in response.AvailablePlugins) {
                        Log($"   - [{plugin.Type}] {plugin.Name} v{plugin.Version} ({plugin.Description})");
                    }

                    AvailablePlugins = response.AvailablePlugins;
                }
                else if (e.Type == GameMessageServerPluginData.Type) {
                    var pluginData = new GameMessageServerPluginData(e.RawData);
                    var plugin = pluginData.PluginDescription;

                    if (InstalledPlugins.TryGetValue(plugin.Id, out var existingPlugin)) {
                        existingPlugin.Dispose();
                        InstalledPlugins.Remove(plugin.Id);
                    }

                    ServerPluginInstanceBase pluginInstance = null;
                    switch (plugin.Type) {
                        case PluginType.Lua:
                            pluginInstance = new LuaServerPluginInstance(plugin, pluginData.Data);
                            break;
                        case PluginType.Decal:
                            pluginInstance = new DecalServerPluginInstance(plugin, pluginData.Data);
                            break;
                        default:
                            Log($"Plugin '{plugin.Name}' was unable to load: Unknown type: {plugin.Type}");
                            break;
                    }

                    InstalledPlugins.Add(plugin.Id, pluginInstance);

                    if (IsLoggedIn || plugin.LoadType == PluginLoadType.Always) {
                        pluginInstance.Run();
                    }
                }
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private void Core_PluginInitComplete(object sender, EventArgs e) {
        }

        private void Core_PluginTermComplete(object sender, EventArgs e) {
            try {
                IsLoggedIn = false;
                var needsToLoad = InstalledPlugins.Values.Where(p => p.Description.LoadType == PluginLoadType.LoggedIn);

                foreach (var plugin in needsToLoad) {
                    plugin.Dispose();
                }
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        protected override void Shutdown() {
            try {
                ui.Dispose();

                Core.PluginInitComplete -= Core_PluginInitComplete;
                Core.PluginTermComplete -= Core_PluginTermComplete;
                Core.FilterInitComplete -= Core_FilterInitComplete;
            }
            catch (Exception ex) {
                Log(ex);
            }
        }
        #endregion

        internal void Log(Exception ex) {
            Log(ex.ToString());
        }

        internal void Log(string message) {
            try {
                File.AppendAllText(System.IO.Path.Combine(AssemblyDirectory, "log.txt"), $"{message}\n");
                CoreManager.Current.Actions.AddChatText(message, 1);
            }
            catch { }
        }
    }
}
