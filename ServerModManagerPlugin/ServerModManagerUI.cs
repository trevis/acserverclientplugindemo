﻿using Decal.Adapter;
using ImGuiNET;
using ServerModManager.Lib;
using System;
using System.Collections.Generic;
using System.Numerics;
using UtilityBelt.Service;
using UtilityBelt.Service.Views;

namespace ServerModManager {
    internal class ServerModManagerUI : IDisposable {
        private readonly FilterCore _core;

        /// <summary>
        /// The UBService Hud
        /// </summary>
        private readonly Hud hud;

        public ServerModManagerUI(FilterCore core) {
            _core = core;

            // Create a new UBService Hud
            hud = UBService.Huds.CreateHud("Server Mod Manager");

            // set to show our icon in the UBService HudBar
            hud.ShowInBar = true;

            // subscribe to the hud render event so we can draw some controls
            hud.OnRender += Hud_OnRender;
            hud.OnPreRender += Hud_OnPreRender;
        }

        private void Hud_OnPreRender(object sender, EventArgs e) {
            ImGui.SetNextWindowSize(new Vector2(500, 250), ImGuiCond.FirstUseEver);
        }

        /// <summary>
        /// Called every time the ui is redrawing.
        /// </summary>
        private void Hud_OnRender(object sender, EventArgs e) {
            try {
                if (ImGui.Button("Refresh List")) {
                    DecalHelpers.DispatchChatToBoxWithPluginIntercept("/queryserverplugins");
                }

                if (ImGui.BeginTable("plugins", 5, ImGuiTableFlags.Resizable | ImGuiTableFlags.BordersInnerV | ImGuiTableFlags.NoSavedSettings)) {
                    ImGui.TableSetupColumn("Name");
                    ImGui.TableSetupColumn("Version");
                    ImGui.TableSetupColumn("Type");
                    ImGui.TableSetupColumn("Description");
                    ImGui.TableSetupColumn("Actions");
                    ImGui.TableHeadersRow();

                    foreach (var plugin in _core.AvailablePlugins) {
                        ImGui.PushID($"plugin-row-{plugin.Id}");
                        ImGui.TableNextRow();

                        ImGui.TableSetColumnIndex(0);
                        ImGui.Text(plugin.Name);

                        ImGui.TableSetColumnIndex(1);
                        ImGui.Text(plugin.Version.ToString());

                        ImGui.TableSetColumnIndex(2);
                        ImGui.Text(plugin.Type.ToString());

                        ImGui.TableSetColumnIndex(3);
                        ImGui.Text(plugin.Description);

                        ImGui.TableSetColumnIndex(4);
                        if (_core.InstalledPlugins.ContainsKey(plugin.Id)) {
                            if (ImGui.Button("Remove")) {
                                _core.RemovePlugin(plugin.Id);
                            }
                        }
                        else {
                            if (ImGui.Button("Install")) {
                                DecalHelpers.DispatchChatToBoxWithPluginIntercept($"/getserverplugin {plugin.Id}");
                            }
                        }
                        ImGui.PopID();
                    }
                }
                ImGui.EndTable();
            }
            catch (Exception ex) {
                _core.Log(ex);
            }
        }

        public void Dispose() {
            hud.Dispose();
        }
    }
}