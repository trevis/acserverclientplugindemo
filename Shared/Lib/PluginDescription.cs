#if NET6_0_OR_GREATER
using ACE.Server.Network.GameMessages;
#endif
#if NET48_OR_GREATER
using UtilityBelt.Common.Enums;
using ServerModManager.Lib;
#endif
using System.IO;
using System;

public class PluginDescription {
    public string Id { get; set; }
    public string Name { get; set; }
    public Version Version { get; set; }
    public string Description { get; set; }
    public PluginType Type { get; set; }
    public PluginLoadType LoadType { get; set; } = PluginLoadType.LoggedIn;
    public PluginDescription(string id, string name, Version version, string description, PluginType type) {
        Id = id;
        Name = name;
        Description = description;
        Version = version;
        Type = type;
    }

#if NET6_0_OR_GREATER
        public byte[] Data { get; set; }
        public void Write(BinaryWriter writer) {
            writer.Write((int)Type);
            writer.WriteString16L(Id);
            writer.WriteString16L(Name);
            writer.WriteString16L(Version.ToString());
            writer.WriteString16L(Description);
        }
#endif

#if NET48_OR_GREATER
    public PluginDescription(BinaryReader reader) {
        Type = (PluginType)reader.ReadInt32();
        Id = BinaryHelpers.ReadString(reader);
        Name = BinaryHelpers.ReadString(reader);
        Decal.Adapter.CoreManager.Current.Actions.AddChatText($"test:{Type} {Name}", 1);
        Version = new Version(BinaryHelpers.ReadString(reader));
        Description = BinaryHelpers.ReadString(reader);
    }
#endif
}