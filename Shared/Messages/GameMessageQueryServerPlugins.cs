#if NET6_0_OR_GREATER
using ACE.Server.Network.GameMessages;
#endif
#if NET48_OR_GREATER
using UtilityBelt.Common.Enums;
#endif
using System.IO;

public class GameMessageQueryServerPlugins
#if NET6_0_OR_GREATER
    : GameMessage
#endif
    {
#if NET6_0_OR_GREATER
    public GameMessageQueryServerPlugins()
        : base((GameMessageOpcode)0xDEAD, GameMessageGroup.UIQueue) {
    }
#endif
#if NET48_OR_GREATER
    public static readonly MessageType Type = (MessageType)0xDEAD;

    public GameMessageQueryServerPlugins(byte[] data) {
        using (var stream = new MemoryStream(data))
        using (var reader = new BinaryReader(stream)) {
            
        }
    }
#endif
}