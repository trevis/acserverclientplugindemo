#if NET6_0_OR_GREATER
using ACE.Server.Network.GameMessages;
#endif
#if NET48_OR_GREATER
using UtilityBelt.Common.Enums;
using ServerModManager.Lib;
#endif
using System.IO;
using System;
using System.Collections.Generic;
using static GameMessageQueryServerPluginsResponse;

public class GameMessageServerPluginData
#if NET6_0_OR_GREATER
    : GameMessage
#endif
    {

#if NET6_0_OR_GREATER
    public GameMessageServerPluginData(PluginDescription plugin)
        : base((GameMessageOpcode)0xDEAF, GameMessageGroup.UIQueue) {
        plugin.Write(Writer);
        Writer.Write(plugin.Data.Length);
        Writer.Write(plugin.Data);
    }
#endif
#if NET48_OR_GREATER
    public static readonly MessageType Type = (MessageType)0xDEAF;

    public List<PluginDescription> AvailablePlugins { get; } = new List<PluginDescription>();
    public byte[] Data { get; }
    public PluginDescription PluginDescription { get; }

    public GameMessageServerPluginData(byte[] data) {
        using (var stream = new MemoryStream(data))
        using (var reader = new BinaryReader(stream)) {
            PluginDescription = new PluginDescription(reader);
            var dataSize = reader.ReadInt32();
            Data = reader.ReadBytes(dataSize);
        }
    }
#endif
}