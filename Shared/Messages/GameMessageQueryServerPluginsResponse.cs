#if NET6_0_OR_GREATER
using ACE.Server.Network.GameMessages;
#endif
#if NET48_OR_GREATER
using UtilityBelt.Common.Enums;
using ServerModManager.Lib;
#endif
using System.IO;
using System;
using System.Collections.Generic;

public class GameMessageQueryServerPluginsResponse
#if NET6_0_OR_GREATER
    : GameMessage
#endif
    {

#if NET6_0_OR_GREATER

    public GameMessageQueryServerPluginsResponse(IEnumerable<PluginDescription> plugins)
        : base((GameMessageOpcode)0xDEAD, GameMessageGroup.UIQueue) {
        Writer.Write(plugins.Count());
        foreach (var plugin in plugins) {
            plugin?.Write(Writer);
        }
    }
#endif
#if NET48_OR_GREATER
    public static readonly MessageType Type = (MessageType)0xDEAD;

    public List<PluginDescription> AvailablePlugins { get; } = new List<PluginDescription>();

    public GameMessageQueryServerPluginsResponse(byte[] data) {
        using (var stream = new MemoryStream(data))
        using (var reader = new BinaryReader(stream)) {
            var pluginCount = reader.ReadInt32();

            for (var i = 0; i < pluginCount; i++) {
                AvailablePlugins.Add(new PluginDescription(reader));
            }
        }
    }
#endif
}