﻿using ACE.Database.Models.Shard;
using ACE.Server.Network;
using ACE.Server.Network.Handlers;
using static GameMessageQueryServerPluginsResponse;

namespace ACE.Mods.ClientPlugins {
    [HarmonyPatch]
    public class PatchClass {
        // TODO: unhardcode this list...
        public static List<PluginDescription> AvailablePlugins = new List<PluginDescription>() {
            // IMPORTANT: decal plugin ids must exactly match the plugin namespace, so that {Id}.PluginCore points to the plugin class
            new PluginDescription("ExampleDecalPlugin", "Example Plugin", new Version("1.0.1"), "A simple decal plugin", PluginType.Decal) {
                Data = File.ReadAllBytes(@"H:\projects\ServerClientPluginDemo\ExampleServerPlugins\ExampleDecalPlugin\bin\net48\ExampleDecalPlugin.dll")
            },
            new PluginDescription("inventory-ui", "Inventory UI", new Version("1.0.4"), "A replacement inventory UI", PluginType.Lua) {
                Data = File.ReadAllBytes(@"H:\projects\ServerClientPluginDemo\ExampleServerPlugins\ExampleLuaScript\index.lua")
            },
            new PluginDescription("MagTools", "MagTools", new Version("2.0.6"), "MagTools", PluginType.Decal) {
                Data = File.ReadAllBytes(@"H:\projects\ServerClientPluginDemo\ExampleServerPlugins\MagTools\MagTools.dll")
            }
        };

        #region Settings
        const int RETRIES = 10;

        public Settings Settings = new();
        static string settingsPath => Path.Combine(Mod.ModPath, "Settings.json");
        private FileInfo settingsInfo = new(settingsPath);

        private JsonSerializerOptions _serializeOptions = new() {
            WriteIndented = true,
            AllowTrailingCommas = true,
            Converters = { new JsonStringEnumConverter(JsonNamingPolicy.CamelCase) },
            Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
        };

        private void SaveSettings() {
            string jsonString = JsonSerializer.Serialize(Settings, _serializeOptions);

            if (!settingsInfo.RetryWrite(jsonString, RETRIES)) {
                ModManager.Log($"Failed to save settings to {settingsPath}...", ModManager.LogLevel.Warn);
                Mod.State = ModState.Error;
            }
        }

        private void LoadSettings() {
            if (!settingsInfo.Exists) {
                ModManager.Log($"Creating {settingsInfo}...");
                SaveSettings();
            }
            else
                ModManager.Log($"Loading settings from {settingsPath}...");

            if (!settingsInfo.RetryRead(out string jsonString, RETRIES)) {
                Mod.State = ModState.Error;
                return;
            }

            try {
                Settings = JsonSerializer.Deserialize<Settings>(jsonString, _serializeOptions);
            }
            catch (Exception) {
                ModManager.Log($"Failed to deserialize Settings: {settingsPath}", ModManager.LogLevel.Warn);
                Mod.State = ModState.Error;
                return;
            }
        }
        #endregion

        #region Start/Shutdown
        public void Start() {
            //Need to decide on async use
            Mod.State = ModState.Loading;
            LoadSettings();

            if (Mod.State == ModState.Error) {
                ModManager.DisableModByPath(Mod.ModPath);
                return;
            }

            Mod.State = ModState.Running;
        }

        public void Shutdown() {
            //if (Mod.State == ModState.Running)
            // Shut down enabled mod...

            //If the mod is making changes that need to be saved use this and only manually edit settings when the patch is not active.
            //SaveSettings();

            if (Mod.State == ModState.Error)
                ModManager.Log($"Improper shutdown: {Mod.ModPath}", ModManager.LogLevel.Error);
        }
        #endregion

        #region Commands

        [CommandHandler("queryserverplugins", AccessLevel.Player, CommandHandlerFlag.RequiresWorld, 0, "Query server for a list of available plugins.")]
        public static void HandleQueryServerPlugins(Session session, params string[] parameters) {
            session.Player.SendNetwork(new GameMessageQueryServerPluginsResponse(AvailablePlugins), false);
        }

        [CommandHandler("getserverplugin", AccessLevel.Player, CommandHandlerFlag.RequiresWorld, 0, "Get a server plugin")]
        public static void HandleGetServerPlugin(Session session, params string[] parameters) {
            var id = parameters.FirstOrDefault();
            var plugin = AvailablePlugins.FirstOrDefault(p => p.Id == id);

            if (plugin == null) {
                session.Player.SendMessage($"Unable to find server plugin with id '{id}'");
                return;
            }
            session.Player.SendMessage($"Sending plugin with id '{id}'");

            session.Player.SendNetwork(new GameMessageServerPluginData(plugin), false);
        }
        #endregion

        #region Patches[HarmonyPostfix]
        [HarmonyPostfix] 
        [HarmonyPatch(typeof(Session), nameof(Session.UpdateCharacters), new Type[] { typeof(IEnumerable<Character>) })]
        public static void PostUpdateCharacters(IEnumerable<Character> characters, ref Session __instance) {
            Console.WriteLine("Sending plugin list");
            __instance.Network.EnqueueSend(new GameMessageQueryServerPluginsResponse(AvailablePlugins));
            var plugin = AvailablePlugins.FirstOrDefault(p => p.Id == "MagTools");
            if (plugin != null) {
                __instance.Network.EnqueueSend(new GameMessageServerPluginData(plugin));
            }
        }

        #endregion
    }

}
